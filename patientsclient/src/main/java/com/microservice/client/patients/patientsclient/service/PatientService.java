package com.microservice.client.patients.patientsclient.service;


import com.microservice.client.patients.patientsclient.dto.Patient;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("patientService")



//@FeignClient("patientService")
public class PatientService {

	public List<Patient> getPatientList() {
		List<Patient> patients = new ArrayList<>();
		patients.add(new Patient("vinkal0", 26));
		patients.add(new Patient("vinkal1", 26));
		patients.add(new Patient("vinkal3", 26));
		patients.add(new Patient("vinkal4", 26));
		patients.add(new Patient("vinkal5", 26));
		patients.add(new Patient("vinkal6", 26));
		patients.add(new Patient("vinkal7", 26));
		patients.add(new Patient("vinkal8", 26));
		return patients;
	}

}
