package com.microservice.client.patients.patientsclient.controller;


import com.microservice.client.patients.patientsclient.dto.Patient;
import com.microservice.client.patients.patientsclient.service.PatientService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
public class PatientController {

	@Autowired
	PatientService patientService;



	@GetMapping("/")
	public String getHome(){
		return  "patients service from port 8082";
	}

	//@HystrixCommand(fallbackMethod = "fallback")
	@GetMapping("/get-patients-list")
	public List<Patient> getPatients() {
		return patientService.getPatientList();
	}


	// a fallback method to be called if failure happened
	public Patient fallback(Throwable hystrixCommand) {
		return new Patient("vinkal kumar",26);

	}

}
