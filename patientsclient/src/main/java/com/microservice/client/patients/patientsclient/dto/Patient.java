package com.microservice.client.patients.patientsclient.dto;


public class Patient {

	private String pName;

	private Integer age;

	public Patient() {
	}

	public Patient(String pName, Integer age) {
		this.pName = pName;
		this.age = age;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
}
